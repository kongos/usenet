/** This database saves the information in memory.
 *  In the header file there is two structs; one
 *  for Articles and one for Newsgroups that can
 *  hold Articles.
 */

#ifndef INMEMORYDATABASE_H
#define INMEMORYDATABASE_H

#include <map>

#include "database.h"

class InMemoryDatabase : public Database
{
public:
    InMemoryDatabase() : Database() {}

    /** This const member loads the data for the specified key
     *  from database.
     *  @param key The key to load
     *  @param data The string reference to be filled in with the data
     */
    void get_key(const string &key, string &data) const
    {
        data = (*db.find(key)).second; // does always exist
    }

    /** This member saves the specified data for the
     *  specified key to database. Any existing data is replaced.
     *  @param key The key to save
     *  @param data The data to save
     */
    void set_key(const string &key, const string &data)
    {
        db[key] = data;
    }

    /** This member deletes the record corresponding to key
     *  from database.
     *  @param key The key to delete
     */
    void delete_key(const string &key)
    {
        db.erase(key);
    }
private:
    std::map<string, string> db;
};

#endif
