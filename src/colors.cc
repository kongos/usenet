/* This class defines possibility to
 * change colors of cout using
 * ANSI escape codes
 */
#include "colors.h"
#include <string>

/** Using its own namespace colors
*/
namespace colors
{

/** This member sets the currentcolor
 *  to the ANSI equavivilent of reset
 *  to terminal standard
 */
void Colors::reset()
{
    currentcolor = "\33[0m";
}

/** This member sets the currentcolor
 *  to the ANSI equavivilent of red
 *  to terminal standard
 */
void Colors::get_red()
{
    currentcolor = "\33[31m";
}

/** This member sets the currentcolor
 *  to the ANSI equavivilent of green
 *  to terminal standard
 */
void Colors::get_green()
{
    currentcolor = "\33[32m";
}

/** This member sets the currentcolor
 *  to the ANSI equavivilent of yellow
 *  to terminal standard
 */
void Colors::get_yellow()
{
    currentcolor = "\33[33m";
}

/** This non member makes it possible for
 *  a Color to be written to ostream using
 *  the normal operator <<
 *  @param output The terminal based ostream to output to
 *  @param c The color object
 *  @return The same ostream
 */
ostream &operator<<(ostream &output, const Colors &c)
{
    output << c.currentcolor;
    return output;
}


}
