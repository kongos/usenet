/** This is the "interface" for our databases
 *  i.e. definitions of functions here are
 *  the same in other versions of databases.
 *
 *  The databases are not thread safe.
 *  The API functions throws exceptions on error.
 */

#ifndef DATABASE_H
#define DATABASE_H

#include <string>
#include <vector>

using std::vector;
using std::string;

/** NGAlreadyInDatabaseException is thrown when a Newsgroup
 *  with that name already is present in the database
 */
struct NGAlreadyInDatabaseException {};

/** NGNotInDatabaseException is thrown when
 *  a Newsgroup cannot be found in the database
 */
struct NGNotInDatabaseException {};

/** Struct ArtNotInDatabaseException is thrown when
 *  an Article cannot be found in the database at
 *  the place the user supplied input for
 */
struct ArtNotInDatabaseException {};

class Database
{
public:
    /* Constructor and destructor */
    Database() : newsgroup_nextid(1), articles_cur(-1) {}
    virtual ~Database();

    void load_database() {
        load_newsgroups();
    }

    /* API functions */
    int get_num_newsgroups() const;
    int get_newsgroup_index(int ng_id) const;
    int get_newsgroup_id(int ng_index) const;
    string get_newsgroup_name(int ng_index) const;

    int create_newsgroup(const string &name);
    void delete_newsgroup(int ng_id);

    int get_num_articles(int ng_index);
    int get_article_index(int ng_index, int art_id);
    int get_article_id(int art_index) const;
    string get_article_title(int art_index) const;
    string get_article_author(int art_index) const;
    string get_article_text(int art_index) const;

    int create_article(int ng_index, const string &title,
                       const string &author, const string &text);
    void delete_article(int ng_index, int art_id);

    /* To be implemented by concrete databases */
    virtual void get_key(const string &key, string &data) const = 0;
    virtual void set_key(const string &key, const string &data) = 0;
    virtual void delete_key(const string &key) = 0;

    /* Representation of an article */
    struct Article
    {
        int id;
        string title;
        string author;

        Article(int aid, const string &atitle, const string &aauthor)
                : id(aid), title(atitle), author(aauthor) {};
    };

    /* Representation of a newsgroup containing articles */
    struct NewsGroup
    {
        int id;
        string name;
        int article_nextid;

        NewsGroup(int aid, const string &aname) : id(aid),
                name(aname), article_nextid(1) {};
    };
private:
    int newsgroup_nextid;
    int article_nextid;
    int articles_cur;
    vector<NewsGroup *> newsgroups;
    vector<Article *> articles;

    void load_newsgroups();
    void save_newsgroups();
    void load_articles(int ng_id);
    void save_articles(int ng_id);
    void delete_articles(int ng_id);

    void load_text(int ng_id, int art_id, string& text) const;
    void save_text(int ng_id, int art_id, const string& text);
    void delete_text(int ng_id, int art_id);

    void articles_clear();
};

#endif
