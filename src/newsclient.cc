/**
 * main file for newsclient: this is the the part of the client
 * that the user will interface with.
 */
#include <vector>
#include <iostream>
#include <cstdlib>

#include "connection.h"
#include "connectionclosedexception.h"
#include "protocolviolationexception.h"
#include "clientcommand.h"
#include "colors.h"

using std::cout;
using std::cerr;
using std::endl;
using std::flush;
using std::cin;
using client_server::Connection;
using client_server::ConnectionClosedException;
using protocol::ProtocolViolationException;
using colors::Colors;

/**
 * This member defines printing of the menu
 */
void printMenu()
{
    cout << "--------------------------------\n"
         << "  Menu\n"
         << "--------------------------------\n"
         << "[1] List newsgroups\n"
         << "[2] Create a newsgroup\n"
         << "[3] Delete a newsgroup\n"
         << "[4] List articles in a newsgroup\n"
         << "[5] Create an article\n"
         << "[6] Delete an article\n"
         << "[7] Get an article\n"
         << "[Ctrl + D] Quit\n"
         << "--------------------------------\n\n"
         << "Your choice: ";
}

/** This is the main in newsclient
 *  @param argc Number of arguments
 *  @param argv Vector with the arguments
 *  @return The system independent macro EXIT_FAILURE on error
 */
int main(int argc, char *argv[])
{
    Colors colors_reset;
    Colors colors_yellow;
    colors_yellow.get_yellow();
    Colors colors_red;
    colors_red.get_red();
    Colors colors_green;
    colors_green.get_green();

    if (argc != 3)
    {
        cerr << colors_yellow
             << "Usage: myclient host-name port-number"
             << colors_reset << endl;
        exit(1);
    }

    cout << "Trying to connect to " << argv[1] << ':' << argv[2] << endl;
    Connection conn(argv[1], atoi(argv[2]));
    if (! conn.isConnected())
    {
        cerr << colors_red
             << "Connection attempt failed"
             << colors_reset << endl;
        exit(1);
    }
    cout << colors_green
         << "Connection attempt success"
         << colors_reset << endl;

    string line;
    printMenu();

    while (getline(cin, line))
    {
        int command = line[0] - '0';
        cout << flush;

        try
        {
            ClientCommand clicmd(&conn);
            clicmd.command(command);
            printMenu();
        }
        catch (ConnectionClosedException &)
        {
            cerr << colors_red
                 << "Server closed down!"
                 << colors_reset << endl;
            return EXIT_FAILURE;
        }
        catch (ProtocolViolationException)
        {
            cerr << colors_red
                 << "Protocol violation!"
                 << colors_reset << endl;
            return EXIT_FAILURE;
        }
    }

    cout << endl;
}
