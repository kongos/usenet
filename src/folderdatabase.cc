/** This class defines storage of articles and newsgroups
 *  to the harddrive. Any updates made to the database
 *  is immediately reflected on the disk.
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>

#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>

#include "folderdatabase.h"

using std::ostringstream;
using std::ifstream;
using std::cerr;
using std::cout;
using std::endl;
using std::ofstream;
using std::ios;

/** A Constructor
 *  If the root folder doesn't exist, create it,
 *  otherwise load the existing database
 *  @param root The root folder the database is stored in
 */
FolderDatabase::FolderDatabase(const string &root)
    : Database(), rootdir(root)
{
    create_rootdir();
    prepare_database();
}

/** This const member loads the data for the specified key
 *  from disk, or terminates the program if the file doesn't exists.
 *  @param key The key to load
 *  @param data The string reference to be filled in with the data
 */
void FolderDatabase::get_key(const string &key, string &data) const {
    ostringstream path;
    path << rootdir << "/" << key << ".db";
    ifstream is(path.str().c_str());
    if (! is)
    {
        cerr << "Error: Database is incomplete" << endl;
        exit(1);
    }

    is.seekg(0, ios::end);
    data.resize(is.tellg());
    is.seekg(0, ios::beg);
    is.read(&data[0], data.size());
}

/** This member saves the specified data for the
 *  specified key to disk. Any existing data is replaced.
 *  @param key The key to save
 *  @param data The data to save
 */
void FolderDatabase::set_key(const string &key, const string &data) {
    ostringstream path;
    path << rootdir << "/" << key << ".db";
    ofstream os(path.str().c_str());

    os << data;
}

/** This member deletes the file and all data
 *  from disk belonging to the specified key.
 *  @param key The key to delete
 */
void FolderDatabase::delete_key(const string &key) {
    ostringstream path;
    path << rootdir << "/" << key << ".db";
    unlink(path.str().c_str());
}

/** This member creates the root folder if
 *  it doesn't already exist
 */
void FolderDatabase::create_rootdir()
{
    DIR *dir = opendir(rootdir.c_str());
    if (dir == NULL)
    {
        cout << "Creating new database folder" << endl;

        int ret = mkdir(rootdir.c_str(), 0755);
        if (ret != 0)
        {
            cerr << "Failed to create database folder" << endl;
            exit(1);
        }
    }
    else
    {
        cout << "Using existing database folder" << endl;
        closedir(dir);
    }
}

/** 
 * This member loads the database if it exists
 */
void FolderDatabase::prepare_database()
{
    ostringstream path;
    path << rootdir << "/newsgroups.db";
    ifstream is(path.str().c_str());
    if (! is) return;

    load_database();
}
