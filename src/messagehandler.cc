/** 
 * Class MessageHandler, is responsible for low level communication between
 * the server and the client
 */
#include "messagehandler.h"
#include "protocol.h"

using protocol::Protocol;
using client_server::Server;

/** This const member writes a byte to the connection
 *  @param value is an int
 *  @throws ConnectionClosedException
 */
void MessageHandler::sendByte(int value) const
throw(ConnectionClosedException)
{
    conn->write(value);
}

/** This const member sends an int via sendByte
 *  @param value is an int
 *  @throws ConnectionClosedException
 */
void MessageHandler::sendInt(int value) const
throw(ConnectionClosedException)
{
    sendByte((value >> 24) & 0xFF);
    sendByte((value >> 16) & 0xFF);
    sendByte((value >> 8) & 0xFF);
    sendByte(value & 0xFF);
}

/** This const member sends an int parameter via sendByte
 *  @param param is an int
 *  @throws ConnectionClosedException
 */
void MessageHandler::sendIntPar(int param) const
throw(ConnectionClosedException)
{
    sendByte(Protocol::PAR_NUM);
    sendInt(param);
}

/** This const member sends a string parameter via sendByte
 *  @param &param is a reference to a const string
 *  @throws ConnectionClosedException
 */
void MessageHandler::sendStringPar(const string &param) const
throw(ConnectionClosedException)
{
    sendByte(Protocol::PAR_STRING);
    sendInt(param.size());
    for (size_t i = 0; i != param.size(); ++i)
    {
        sendByte(param[i]);
    }
}

/** This const member reads a Byte from the connection
 *  @throws ConnectionClosedException
 */
int MessageHandler::readByte() const
throw(ConnectionClosedException)
{
    return conn->read();
}

/** This const member reads an int from the connection
 *  @return b1 is an int
 *  @return b2 is an int
 *  @return b3 is an int
 *  @return b4 is an int
 *  @throws ConnectionClosedException
 */
int MessageHandler::readInt() const
throw(ConnectionClosedException)
{
    int b1 = readByte();
    int b2 = readByte();
    int b3 = readByte();
    int b4 = readByte();

    return b1 << 24 | b2 << 16 | b3 << 8 | b4;
}

/** This const member reads an int parameter from the connection
 *  @return readInt() is a member returning an int
 *  @throws ProtocolViolationException
 */
int MessageHandler::readIntPar() const
throw(ProtocolViolationException)
{
    if (readByte() != Protocol::PAR_NUM)
    {
        throw ProtocolViolationException();
    }
    return readInt();
}

/** This const member reads a string parameter from the connection
 *  @return result is a string
 *  @throws ProtocolViolationException
 */
string MessageHandler::readStringPar() const
throw(ProtocolViolationException)
{
    if (readByte() != Protocol::PAR_STRING)
    {
        throw ProtocolViolationException();
    }

    int n = readInt();
    if (n < 0)
    {
        throw ProtocolViolationException();
    }

    string result;
    for (int i = 0; i != n; ++i)
    {
        result += conn->read();
    }

    return result;
}
