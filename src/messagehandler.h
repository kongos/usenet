#ifndef MESSAGEHANDLER_H
#define MESSAGEHANDLER_H

#include <string>

#include "connection.h"
#include "connectionclosedexception.h"
#include "protocolviolationexception.h"

using std::string;
using client_server::Connection;
using client_server::ConnectionClosedException;
using protocol::ProtocolViolationException;

class MessageHandler
{
public:
    MessageHandler(Connection *aconn) : conn(aconn) {};

    void sendByte(int value) const
    throw(ConnectionClosedException);
    void sendInt(int value) const
    throw(ConnectionClosedException);
    void sendIntPar(int param) const
    throw(ConnectionClosedException);
    void sendStringPar(const string &param) const
    throw(ConnectionClosedException);

    int readByte() const
    throw(ConnectionClosedException);
    int readInt() const
    throw(ConnectionClosedException);
    int readIntPar() const
    throw(ProtocolViolationException);
    string readStringPar() const
    throw(ProtocolViolationException);
private:
    Connection *conn;
};

#endif
