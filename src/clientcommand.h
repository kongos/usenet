/*
 * Class ClientCommand: contains definitions of commands availible in the
 * client. This class can prepare, send and recieve messages according to
 * the protocol defined in "protocol.h"
 */
#ifndef CLIENTCOMMAND_H
#define CLIENTCOMMAND_H

#include <string>
#include <vector>
#include <cstdlib>
 
#include "connection.h"
#include "connectionclosedexception.h"
#include "colors.h"
#include "messagehandler.h"

using std::string;
using std::vector;
using client_server::Connection;
using client_server::ConnectionClosedException;
using colors::Colors;


/**
 * Enum to use internally for making a more readable code
 */
enum errorsInClientCommand
{
    TOSHORTTITLE, TOSHORTAUTHOR, SERVERISDOWN
};

class ClientCommand
{
public:
    ClientCommand(Connection *aconn) : conn(aconn) {};
    void command(int command);
private:
    Connection *conn; // is a pointer to a Connection object

    void list_ng(MessageHandler &mh) const;
    void create_ng(MessageHandler &mh, const string &title) const;
    void delete_ng(MessageHandler &mh, int news_id) const;
    void list_art(MessageHandler &mh, int news_id) const;
    void create_art(MessageHandler &mh, int news_id, const string &title,
                   const string &author, const string &text) const;
    void delete_art(MessageHandler &mh, int news_id, int art_id) const;
    void get_art(MessageHandler &mh, int news_id, int art_id) const;
    void print_article(const string &title, const string &author,
                      const string &text) const;

    int get_valid_int(const string &prompt);
    string get_valid_text(const string &prompt);
    void handle_error(int errcode) const;
};

#endif
