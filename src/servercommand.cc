/** This class defines and declares functionability in the server
 */
#include <iostream>

#include "servercommand.h"
#include "protocol.h"

using std::cout;
using std::endl;

using protocol::Protocol;

/** This const member checks the incomming command against the protocol
 *  then calles the correct function for handling the interaction
 *  @param *conn is a connection pointer
 *  @return boolean
 */
bool ServerCommand::handle(Connection *conn) const
{
    MessageHandler mh(conn);

    try
    {
        switch (mh.readByte())
        {
        case Protocol::COM_LIST_NG:
            return handle_listng(mh);
        case Protocol::COM_CREATE_NG:
            return handle_createng(mh);
        case Protocol::COM_DELETE_NG:
            return handle_deleteng(mh);
        case Protocol::COM_LIST_ART:
            return handle_listart(mh);
        case Protocol::COM_CREATE_ART:
            return handle_createart(mh);
        case Protocol::COM_DELETE_ART:
            return handle_deleteart(mh);
        case Protocol::COM_GET_ART:
            return handle_getart(mh);
        default:
            return false;
        }
    }
    catch (ConnectionClosedException)
    {
    }

    return false;
}

/** This const member handles the command list newsgroups
 *  @param &mh is reference to a MessageHandlers
 *  @return boolean
 */
bool ServerCommand::handle_listng(MessageHandler &mh) const
{
    cout << "List all newsgroups" << endl;

    if (mh.readByte() != Protocol::COM_END) return false;

    int num = db->get_num_newsgroups();
    mh.sendByte(Protocol::ANS_LIST_NG);
    mh.sendIntPar(num);

    for (int i = 0; i != num; ++i)
    {
        mh.sendIntPar(db->get_newsgroup_id(i));
        mh.sendStringPar(db->get_newsgroup_name(i));
    }

    mh.sendByte(Protocol::ANS_END);
    return true;
}

/** This const member handles the command create newsgroup
 *  @param &mh is reference to a MessageHandlers
 *  @return boolean
 */
bool ServerCommand::handle_createng(MessageHandler &mh) const
{
    cout << "Create newsgroup" << endl;

    string name = mh.readStringPar();
    if (mh.readByte() != Protocol::COM_END) return false;

    mh.sendByte(Protocol::ANS_CREATE_NG);

    try
    {
        db->create_newsgroup(name);
        mh.sendByte(Protocol::ANS_ACK);
    }
    catch (NGAlreadyInDatabaseException)
    {
        mh.sendByte(Protocol::ANS_NAK);
        mh.sendByte(Protocol::ERR_NG_ALREADY_EXISTS);
        cout << "ERR_NG_ALREADY_EXISTS" << endl;
    }

    mh.sendByte(Protocol::ANS_END);
    return true;
}

/** This const member handles the command delete newsgroup
 *  @param &mh is reference to a MessageHandlers
 *  @return boolean
 */
bool ServerCommand::handle_deleteng(MessageHandler &mh) const
{
    cout << "Delete newsgroup" << endl;

    int ng_id = mh.readIntPar();
    if (mh.readByte() != Protocol::COM_END) return false;

    mh.sendByte(Protocol::ANS_DELETE_NG);

    try
    {
        db->delete_newsgroup(ng_id);
        mh.sendByte(Protocol::ANS_ACK);
    }
    catch (NGNotInDatabaseException)
    {
        mh.sendByte(Protocol::ANS_NAK);
        mh.sendByte(Protocol::ERR_NG_DOES_NOT_EXIST);
        cout << "ERR_NG_DOES_NOT_EXIST" << endl;
    }

    mh.sendByte(Protocol::ANS_END);
    return true;
}

/** This const member handles the command list articles
 *  @param &mh is reference to a MessageHandlers
 *  @return boolean
 */
bool ServerCommand::handle_listart(MessageHandler &mh) const
{
    cout << "List articles in newsgroup" << endl;

    int ng_id = mh.readIntPar();
    if (mh.readByte() != Protocol::COM_END) return false;

    mh.sendByte(Protocol::ANS_LIST_ART);

    try
    {
        int ng_index = db->get_newsgroup_index(ng_id);
        int num = db->get_num_articles(ng_index);

        mh.sendByte(Protocol::ANS_ACK);
        mh.sendIntPar(num);

        for (int i = 0; i < num; ++i)
        {
            mh.sendIntPar(db->get_article_id(i));
            mh.sendStringPar(db->get_article_title(i));
        }
    }
    catch (NGNotInDatabaseException)
    {
        mh.sendByte(Protocol::ANS_NAK);
        mh.sendByte(Protocol::ERR_NG_DOES_NOT_EXIST);
        cout << "ERR_NG_DOES_NOT_EXIST" << endl;
    }

    mh.sendByte(Protocol::ANS_END);
    return true;
}

/** This const member handles the command create article
 *  @param &mh is reference to a MessageHandlers
 *  @return boolean
 */
bool ServerCommand::handle_createart(MessageHandler &mh) const
{
    cout << "Create article" << endl;

    int ng_id = mh.readIntPar();
    string title = mh.readStringPar();
    string author = mh.readStringPar();
    string text = mh.readStringPar();
    if (mh.readByte() != Protocol::COM_END) return false;

    mh.sendByte(Protocol::ANS_CREATE_ART);

    try
    {
        int ng_index = db->get_newsgroup_index(ng_id);
        db->create_article(ng_index, title, author, text);

        mh.sendByte(Protocol::ANS_ACK);
    }
    catch (NGNotInDatabaseException)
    {
        mh.sendByte(Protocol::ANS_NAK);
        mh.sendByte(Protocol::ERR_NG_DOES_NOT_EXIST);
        cout << "ERR_NG_DOES_NOT_EXIST" << endl;
    }

    mh.sendByte(Protocol::ANS_END);
    return true;
}

/** This const member handles the command delete article
 *  @param &mh is reference to a MessageHandlers
 *  @return boolean
 */
bool ServerCommand::handle_deleteart(MessageHandler &mh) const
{
    cout << "Delete article" << endl;

    int ng_id = mh.readIntPar();
    int art_id = mh.readIntPar();
    if (mh.readByte() != Protocol::COM_END) return false;

    mh.sendByte(Protocol::ANS_DELETE_ART);

    try
    {
        int ng_index = db->get_newsgroup_index(ng_id);
        db->delete_article(ng_index, art_id);

        mh.sendByte(Protocol::ANS_ACK);
    }
    catch (NGNotInDatabaseException)
    {
        mh.sendByte(Protocol::ANS_NAK);
        mh.sendByte(Protocol::ERR_NG_DOES_NOT_EXIST);
        cout << "ERR_NG_DOES_NOT_EXIST" << endl;
    }
    catch (ArtNotInDatabaseException)
    {
        mh.sendByte(Protocol::ANS_NAK);
        mh.sendByte(Protocol::ERR_ART_DOES_NOT_EXIST);
        cout << "ERR_ART_DOES_NOT_EXIST" << endl;
    }

    mh.sendByte(Protocol::ANS_END);
    return true;
}

/** This const member handles the command get article
 *  @param &mh is reference to a MessageHandlers
 *  @return boolean
 */
bool ServerCommand::handle_getart(MessageHandler &mh) const
{
    cout << "Get article" << endl;

    int ng_id = mh.readIntPar();
    int art_id = mh.readIntPar();
    if (mh.readByte() != Protocol::COM_END) return false;

    mh.sendByte(Protocol::ANS_GET_ART);

    try
    {
        int ng_index = db->get_newsgroup_index(ng_id);
        int art_index = db->get_article_index(ng_index, art_id);

        mh.sendByte(Protocol::ANS_ACK);
        mh.sendStringPar(db->get_article_title(art_index));
        mh.sendStringPar(db->get_article_author(art_index));
        mh.sendStringPar(db->get_article_text(art_index));
    }
    catch (NGNotInDatabaseException)
    {
        mh.sendByte(Protocol::ANS_NAK);
        mh.sendByte(Protocol::ERR_NG_DOES_NOT_EXIST);
        cout << "ERR_NG_DOES_NOT_EXIST" << endl;
    }
    catch (ArtNotInDatabaseException)
    {
        mh.sendByte(Protocol::ANS_NAK);
        mh.sendByte(Protocol::ERR_ART_DOES_NOT_EXIST);
        cout << "ERR_ART_DOES_NOT_EXIST" << endl;
    }

    mh.sendByte(Protocol::ANS_END);
    return true;
}
