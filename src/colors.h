/* This class defines possibility to
 * change colors of cout using
 * ANSI escape codes
 */
#ifndef COLORS_H
#define COLORS_H

#include <string>
#include <ostream>

using std::string;
using std::ostream;

/**
 * Using its own namespace colors
 */
namespace colors
{
class Colors
{
    /** Declaring ostream &operator<< as a friend class, enabling printing
     *  Colors with <<operator
     */
    friend ostream &operator<<(ostream &output, const Colors &c);
public:
    /** A constructor initializing the currentcolor to
     *  the ASCII escape sequence that makes stdout print
     *  with its default colors
     */
    Colors() : currentcolor("\33[0m") {};

    void reset();
    void get_red();
    void get_green();
    void get_yellow();
private:
    string currentcolor; // is a const string holding the current color
};
}
#endif
