/** Functions that is the same for all kind of databases.
 *  This includes how you find, list and update newsgroups
 *  and articles, as well as the actual database format,
 *  but not how or where the database is stored.
 *
 *  A temporary index is used in the functions below for
 *  both newsgroups and articles, to speed up access.
 *  These indices are invalidated whenever a change is done
 *  to the database, and must be looked up anew after that.
 */

#include <sstream>

#include "database.h"

using namespace std;


/*
 *  Destructor deletes the cached data
 */
Database::~Database() {
    for (vector<NewsGroup *>::iterator it = newsgroups.begin();
            it != newsgroups.end(); ++it)
    {
        delete *it;
    }

    articles_clear();
}

/*
 *  PUBLIC FUNCTIONS
 */

/** Get number of newsgroups in the database.
 *  The newsgroups can then be accessed individually
 *  knowing that the temporary indices is listed from
 *  0 up to one less than the returned value.
 *  @return Number of newsgroups
 */
int Database::get_num_newsgroups() const
{
    return newsgroups.size();
}

/** Convert a newsgroups unique ID into the temporary
 *  index used in the cache for faster database accesses.
 *  @param ng_id Newsgroups ID
 *  @return Corresponding temporary index
 *  @throws NGNotInDatabaseException if no such newsgroup
 */
int Database::get_newsgroup_index(int ng_id) const
{
    for (size_t i = 0; i != newsgroups.size(); ++i)
    {
        if (newsgroups[i]->id == ng_id) return i;
    }

    throw NGNotInDatabaseException();
}

/** Get the unique ID for a newsgroup
 *  @param ng_index The temporary newsgroup index
 *  @return The newsgroups unique ID
 */
int Database::get_newsgroup_id(int ng_index) const
{
    return newsgroups[ng_index]->id;
}

/** Get the name of a newsgroup
 *  @param ng_index The temporary newsgroup index
 *  @return A string copy of the newsgroups name
 */
string Database::get_newsgroup_name(int ng_index) const
{
    return newsgroups[ng_index]->name;
}

/** Create a new newsgroup in the database.
 *  All temporary indices are invalidated by this.
 *  @param name The name to give the new newsgroup
 *  @return The unique ID of the newly created newsgroup
 *  @throws NGAlreadyInDatabaseException if that name already is in use
 */
int Database::create_newsgroup(const string &name)
{
    for (vector<NewsGroup *>::const_iterator it = newsgroups.begin();
            it != newsgroups.end(); ++it)
    {
        NewsGroup *ng = *it;

        if (ng->name == name) throw NGAlreadyInDatabaseException();
    }

    NewsGroup *newng = new NewsGroup(newsgroup_nextid, name);
    newsgroups.push_back(newng);
    newsgroup_nextid++;
    save_newsgroups();

    articles_clear();
    article_nextid = 1;
    articles_cur = newng->id;
    save_articles(newng->id);

    return newng->id;
}

/** Delete a newsgroup from the database.
 *  All temporary indices are invalidated by this.
 *  @param ng_id The newsgroups unique ID
 *  @throws NGNotInDatabaseException if no such newsgroup
 */
void Database::delete_newsgroup(int ng_id)
{
    for (vector<NewsGroup *>::iterator it = newsgroups.begin();
            it != newsgroups.end(); ++it)
    {
        NewsGroup *ng = *it;

        if (ng->id == ng_id)
        {
            newsgroups.erase(it);
            save_newsgroups();

            load_articles(ng_id);
            for (vector<Article *>::iterator it2 = articles.begin();
                    it2 != articles.end(); ++it2)
            {
                Article *art = *it2;
                delete_text(ng_id, art->id);
            }
            delete_articles(ng_id);

            delete ng;
            return;
        }
    }

    throw NGNotInDatabaseException();
}

/** Get the number of articles in a specific newsgroup.
 *  This ensures this newsgroup is in the cache.
 *  The articles can then be accessed individually
 *  knowing that the temporary indices is listed from
 *  0 up to one less than the returned value.
 *  @param ng_index The temporary newsgroup index
 *  @return Number of articles
 */
int Database::get_num_articles(int ng_index)
{
    load_articles(newsgroups[ng_index]->id);
    return articles.size();
}

/** Convert an article unique ID into the temporary
 *  index used in the cache for faster database accesses.
 *  This ensures this newsgroup is in the cache.
 *  @param ng_index The temporary newsgroup index
 *  @param art_id Article ID
 *  @return Corresponding temporary article index
 *  @throws ArtNotInDatabaseException if no such article
 */
int Database::get_article_index(int ng_index, int art_id)
{
    load_articles(newsgroups[ng_index]->id);

    for (size_t i = 0; i != articles.size(); ++i)
    {
        if (articles[i]->id == art_id) return i;
    }

    throw ArtNotInDatabaseException();
}

/** Get the unique ID for an article.
 *  The ID is unique only inside the newsgroup.
 *  @param art_index The temporary article index
 *  @return The articles unique ID
 */
int Database::get_article_id(int art_index) const
{
    return articles[art_index]->id;
}

/** Get the title of an article
 *  @param art_index The temporary article index
 *  @return A string copy of the article title
 */
string Database::get_article_title(int art_index) const
{
    return articles[art_index]->title;
}

/** Get the author of an article
 *  @param art_index The temporary article index
 *  @return A string copy of the article author
 */
string Database::get_article_author(int art_index) const
{
    return articles[art_index]->author;
}

/** Get the text of an article
 *  @param art_index The temporary article index
 *  @return A string copy of the article text
 */
string Database::get_article_text(int art_index) const
{
    string ret;
    load_text(articles_cur, articles[art_index]->id, ret);
    return ret;
}

/** Create a new article inside a newsgroup.
 *  All temporary article indices are invalidated by this.
 *  @param ng_index The temporary index for the newsgroup to post in
 *  @param title Title of the new article
 *  @param author Article authors name
 *  @param text The article text
 *  @return The unique ID of the newly created article
 */
int Database::create_article(int ng_index, const string &title,
                             const string &author, const string &text)
{
    load_articles(newsgroups[ng_index]->id);

    Article *art = new Article(article_nextid, title, author);
    articles.push_back(art);
    article_nextid++;
    save_articles(newsgroups[ng_index]->id);

    save_text(newsgroups[ng_index]->id, art->id, text);

    return art->id;
}

/** Delete an article from a newsgroup.
 *  All temporary article indices are invalidated by this.
 *  @param ng_index The temporary index for the newsgroup to delete from
 *  @param art_id The articles unique ID
 *  @throws ArtNotInDatabaseException if no such article
 */
void Database::delete_article(int ng_index, int art_id)
{
    load_articles(newsgroups[ng_index]->id);

    for (vector<Article *>::iterator it = articles.begin();
            it != articles.end(); ++it)
    {
        Article *art = *it;

        if (art->id == art_id)
        {
            articles.erase(it);
            save_articles(newsgroups[ng_index]->id);
            delete_text(newsgroups[ng_index]->id, art_id);

            delete art;
            return;
        }
    }

    throw ArtNotInDatabaseException();
}

/*
 *  PRIVATE FUNCTIONS
 */

/** This member creates a cache, holding info about newsgroups
 */
void Database::load_newsgroups()
{
    string data;
    get_key("newsgroups", data);
    istringstream is(data);

    int num = is.get() << 24 | is.get() << 16
              | is.get() << 8 | is.get();
    newsgroup_nextid = is.get() << 24 | is.get() << 16
                       | is.get() << 8 | is.get();

    for (int i = 0; i != num; ++i)
    {
        int ng_id = is.get() << 24 | is.get() << 16
                    | is.get() << 8 | is.get();
        int namelen = is.get() << 24 | is.get() << 16
                      | is.get() << 8 | is.get();

        string name;
        name.resize(namelen);
        is.read(&name[0], namelen);

        NewsGroup *ng = new NewsGroup(ng_id, name);
        newsgroups.push_back(ng);
    }
}

/** This member saves the newsgroup cache
 */
void Database::save_newsgroups()
{
    ostringstream os;

    int num = newsgroups.size();
    os << static_cast<char>((num >> 24) & 0xFF);
    os << static_cast<char>((num >> 16) & 0xFF);
    os << static_cast<char>((num >> 8) & 0xFF);
    os << static_cast<char>(num & 0xFF);
    os << static_cast<char>((newsgroup_nextid >> 24) & 0xFF);
    os << static_cast<char>((newsgroup_nextid >> 16) & 0xFF);
    os << static_cast<char>((newsgroup_nextid >> 8) & 0xFF);
    os << static_cast<char>(newsgroup_nextid & 0xFF);

    for (vector<NewsGroup *>::const_iterator it = newsgroups.begin();
            it != newsgroups.end(); ++it)
    {
        NewsGroup *ng = *it;

        os << static_cast<char>((ng->id >> 24) & 0xFF);
        os << static_cast<char>((ng->id >> 16) & 0xFF);
        os << static_cast<char>((ng->id >> 8) & 0xFF);
        os << static_cast<char>(ng->id & 0xFF);
        int namelen = ng->name.size();
        os << static_cast<char>((namelen >> 24) & 0xFF);
        os << static_cast<char>((namelen >> 16) & 0xFF);
        os << static_cast<char>((namelen >> 8) & 0xFF);
        os << static_cast<char>(namelen & 0xFF);
        os << ng->name;
    }

    set_key("newsgroups", os.str());
}

/** This member creates a cache, holding info about articles
 *  in a certain newsgroup
 */
void Database::load_articles(int ng_id)
{
    if (articles_cur == ng_id) return;

    ostringstream key;
    key << "articles_" << ng_id;
    string data;
    get_key(key.str(), data);
    istringstream is(data);

    int num = is.get() << 24 | is.get() << 16
              | is.get() << 8 | is.get();
    article_nextid = is.get() << 24 | is.get() << 16
                     | is.get() << 8 | is.get();

    articles_clear();
    articles_cur = ng_id;

    for (int i = 0; i != num; ++i)
    {
        int art_id = is.get() << 24 | is.get() << 16
                     | is.get() << 8 | is.get();
        int titlelen = is.get() << 24 | is.get() << 16
                       | is.get() << 8 | is.get();
        int authorlen = is.get() << 24 | is.get() << 16
                        | is.get() << 8 | is.get();

        string title;
        title.resize(titlelen);
        is.read(&title[0], titlelen);
        string author;
        author.resize(authorlen);
        is.read(&author[0], authorlen);

        Article *art = new Article(art_id, title, author);
        articles.push_back(art);
    }
}

/** This member saves the cache, holding info about articles
 *  in a certain newsgroup
 */
void Database::save_articles(int ng_id)
{
    ostringstream os;

    int num = articles.size();
    os << static_cast<char>((num >> 24) & 0xFF);
    os << static_cast<char>((num >> 16) & 0xFF);
    os << static_cast<char>((num >> 8) & 0xFF);
    os << static_cast<char>(num & 0xFF);
    os << static_cast<char>((article_nextid >> 24) & 0xFF);
    os << static_cast<char>((article_nextid >> 16) & 0xFF);
    os << static_cast<char>((article_nextid >> 8) & 0xFF);
    os << static_cast<char>(article_nextid & 0xFF);

    for (vector<Article *>::const_iterator it = articles.begin();
            it != articles.end(); ++it)
    {
        Article *art = *it;

        os << static_cast<char>((art->id >> 24) & 0xFF);
        os << static_cast<char>((art->id >> 16) & 0xFF);
        os << static_cast<char>((art->id >> 8) & 0xFF);
        os << static_cast<char>(art->id & 0xFF);
        int titlelen = art->title.size();
        os << static_cast<char>((titlelen >> 24) & 0xFF);
        os << static_cast<char>((titlelen >> 16) & 0xFF);
        os << static_cast<char>((titlelen >> 8) & 0xFF);
        os << static_cast<char>(titlelen & 0xFF);
        int authorlen = art->author.size();
        os << static_cast<char>((authorlen >> 24) & 0xFF);
        os << static_cast<char>((authorlen >> 16) & 0xFF);
        os << static_cast<char>((authorlen >> 8) & 0xFF);
        os << static_cast<char>(authorlen & 0xFF);
        os << art->title;
        os << art->author;
    }

    ostringstream key;
    key << "articles_" << ng_id;
    set_key(key.str(), os.str());
}

/** This member deletes a newsgroup's article database,
 *  but not the article texts
 *  @param ng_id The newsgroups id
 */
void Database::delete_articles(int ng_id)
{
    ostringstream key;
    key << "articles_" << ng_id;
    delete_key(key.str());
}

/** This const member loads the text for an article
 *  @param ng_id Newsgroup the article is in
 *  @param art_id The article id
 *  @param text String the text will be loaded to
 */
void Database::load_text(int ng_id, int art_id, string &text) const
{
    ostringstream key;
    key << "text_" << ng_id << "_" << art_id;
    get_key(key.str(), text);
}

/** This member saves the text for an article
 *  @param ng_id Newsgroup the article is in
 *  @param art_id The article id
 *  @param text The article text to save
 */
void Database::save_text(int ng_id, int art_id, const string &text)
{
    ostringstream key;
    key << "text_" << ng_id << "_" << art_id;
    set_key(key.str(), text);
}

/** This member deletes the text for an article
 *  @param ng_id Newsgroup the article is in
 *  @param art_id The article id
 */
void Database::delete_text(int ng_id, int art_id)
{
    ostringstream key;
    key << "text_" << ng_id << "_" << art_id;
    delete_key(key.str());
}

/** This member deletes all currently loaded
 *  articles from the cache
 */
void Database::articles_clear()
{
    for (vector<Article *>::iterator it = articles.begin();
            it != articles.end(); ++it)
    {
        delete *it;
    }

    articles.clear();
}
