/**
 * main file for newsserver_mem: this class defines and declares the main function
 * for a newsserver that stores articles and newsgroups on the harddrive.
 */
#include <iostream>
#include <cstdlib>
#include <signal.h>

#include "server.h"
#include "inmemorydatabase.h"
#include "servercommand.h"
#include "colors.h"

using std::cout;
using std::endl;
using std::cerr;

using client_server::Server;
using client_server::Connection;
using colors::Colors;

#ifdef DEBUG
bool isrunning;

/** This member handles the SIGINT signal (CTRL+C).
 *  It is only used when debugging memory leaks,
 *  since server::waitForActivity() prevents it from
 *  shutting down immediately.
 */
void sig_handler(int s)
{
    Colors red;
    Colors reset;
    red.get_red();
    cout << red
         << "Caught signal (" << s << ") ... shutting down"
         << reset << endl;

    isrunning = false;
}
#endif

/** This is the main in newsserver_mem
 *  @param argc Number of arguments
 *  @param argv Vector with the arguments
 */
int main(int argc, char *argv[])
{
    Colors colors_reset;
    Colors colors_yellow;
    colors_yellow.get_yellow();
    Colors colors_red;
    colors_red.get_red();
    Colors colors_green;
    colors_green.get_green();

    if (argc != 2)
    {
        cerr << colors_yellow
             << "Usage: " << argv[0]
             << " port-number"
             << colors_reset << endl;
        exit(1);
    }

    int port = atoi(argv[1]);

    Server server(port);
    if (! server.isReady())
    {
        cerr << colors_red
             << "Server initialization error"
             << colors_reset << endl;
        exit(1);
    }

    cout << colors_green
         << "Server ready for incoming connections"
         << colors_reset << endl;

    InMemoryDatabase db;
    ServerCommand srvcmd(&db);

#ifdef DEBUG
    isrunning = true;
    signal(SIGINT, sig_handler); // register sigint i.e ctrl + c

    while (isrunning)
#else
    while (true)
#endif
    {
        Connection *conn = server.waitForActivity();
        if (conn != 0)
        {
            bool ok = srvcmd.handle(conn);

            if (! ok)
            {
                cout << colors_yellow
                     << "Peer didn't obey protocol or disconnected!"
                     << colors_reset << endl;
                server.deregisterConnection(conn);
                delete conn;
            }
        }
        else
        {
            server.registerConnection(new Connection);
            cout << colors_green
                 << "New client connects"
                 << colors_reset << endl;
        }
    }
}
