/** This class defines storage of articles and newsgroups
 *  to the harddrive. Any updates made to the database
 *  is immediately reflected on the disk.
 */

#ifndef FOLDERDATABASE_H
#define FOLDERDATABASE_H

#include "database.h"

class FolderDatabase : public Database
{
public:
    FolderDatabase(const string &root);

    void get_key(const string &key, string &data) const;
    void set_key(const string &key, const string &data);
    void delete_key(const string &key);
private:
    string rootdir;

    void create_rootdir();
    void prepare_database();
};

#endif
