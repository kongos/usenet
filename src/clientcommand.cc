/** This class ClientCommand defines and declares functions can be issued in
 *  the client and functionality that is needed
 */

#include <iostream>
#include <sstream>

#include "clientcommand.h"
#include "protocol.h"

using std::cout;
using std::cerr;
using std::endl;
using std::flush;
using std::cin;
using std::istringstream;
using std::ostringstream;

using protocol::Protocol;
using client_server::Connection;


/** 
 *  This member prepares the message to be sent to the server
 *  with the parameters needed for that specific message
 *  @param command is an integer corresponding with avalible commands
 */
void ClientCommand::command(int command)
{
    string title;
    string author;
    string text;
    int art_id;
    int news_id;

    MessageHandler mh(conn);

    switch (command)
    {
    case Protocol::COM_LIST_NG:
        list_ng(mh);
        break;
    case Protocol::COM_CREATE_NG:
        cout << "Create a newsgroup with title: ";
        getline(cin, title);
        if (title.size() <= 0)
        {
            handle_error(TOSHORTTITLE);
            break;
        }

        create_ng(mh, title);
        cout << endl;
        break;
    case Protocol::COM_DELETE_NG:
        list_ng(mh);
        news_id = get_valid_int("Delete a newsgroup with id");
        delete_ng(mh, news_id);
        cout << endl;
        break;
    case Protocol::COM_LIST_ART:
        list_ng(mh);
        news_id = get_valid_int("List articles in a newsgroup with id");
        cout << endl;
        list_art(mh, news_id);
        break;
    case Protocol::COM_CREATE_ART:
        list_ng(mh);
        news_id = get_valid_int("Create an article within group with id");
        cout << endl;

        cout << "Title: ";
        getline(cin, title);
        if (title.size() <= 0)
        {
            handle_error(TOSHORTTITLE);
            break;
        }

        cout << "Author: ";
        getline(cin, author);
        if (author.size() <= 0)
        {
            handle_error(TOSHORTAUTHOR);
            break;
        }

        cout << endl;
        text = get_valid_text("Message text");

        create_art(mh, news_id, title, author, text);
        cout << endl;
        break;
    case Protocol::COM_DELETE_ART:
        list_ng(mh);
        news_id = get_valid_int("Delete an article with newsgroup id");
        cout << endl;
        list_art(mh, news_id);
        art_id = get_valid_int("And article id");
        delete_art(mh, news_id, art_id);
        cout << endl;
        break;
    case Protocol::COM_GET_ART:
        list_ng(mh);
        news_id = get_valid_int("Get an article with newsgroup id");
        cout << endl;
        list_art(mh, news_id);
        art_id = get_valid_int("And article id");
        cout << endl;
        get_art(mh, news_id, art_id);
        break;
    default:
    {
        Colors color_reset;
        Colors color_yellow;
        color_yellow.get_yellow();

        cout << color_yellow
             << "Unknown/unsupported command"
             << color_reset << endl;
    }
    }

    string line;
    cout << "-- Press ENTER to continue --" << endl;
    getline(cin, line);
}

/** a const member taking three arguments and printing
 *  them formatted as an article
 *  @param &title is a reference to a const string 
 *  @param &author is a reference to a const string
 *  @param &text is a reference to a const string
 */
void ClientCommand::print_article(const string &title, const string &author,
                                 const string &text) const
{
    Colors color_reset;
    Colors color_green;
    color_green.get_green();

    cout << "--------------------------------\n"
         << color_green
         << "Title: " << color_reset
         << title << '\t'
         << color_green
         << "Author: " << color_reset
         << author << '\n'
         << "--------------------------------\n"
         << text << '\n'
         << "--------------------------------\n"
         << flush;
}

/** This const member lists all newsgroups with their
 *  id and their names currently avalible.
 *  @param &mh is a reference to a MessageHandler
 */
void ClientCommand::list_ng(MessageHandler &mh) const
{
    try
    {
        mh.sendByte(Protocol::COM_LIST_NG);
        mh.sendByte(Protocol::COM_END);

        if (mh.readByte() == Protocol::ANS_LIST_NG)
        {
            Colors color_reset;
            Colors color_green;
            color_green.get_green();

            cout << color_green
                 << "Listing all newsgroups"
                 << color_reset << endl;

            int n = mh.readIntPar();
            if (n == 0) cout << " (none)" << endl;

            for (int j = 0; j != n; ++j)
            {
                int id = mh.readIntPar();
                string name = mh.readStringPar();
                cout << " " << id << ": " << name << endl;
            }
            cout << endl;

            if (mh.readByte() == Protocol::ANS_END) return;
        }

        throw ProtocolViolationException();
    }
    catch (ConnectionClosedException)
    {
        handle_error(SERVERISDOWN);
    }
}

/** This const member sends intention to create
 *  a newsgroup with its name.
 *  @param &mh is a reference to a MessageHandler
 *  @param &title is a reference to a const string
 */
void ClientCommand::create_ng(MessageHandler &mh, const string &title) const
{
    try
    {
        mh.sendByte(Protocol::COM_CREATE_NG);
        mh.sendStringPar(title);
        mh.sendByte(Protocol::COM_END);

        if (mh.readByte() == Protocol::ANS_CREATE_NG)
        {
            if (mh.readByte() == Protocol::ANS_ACK)
            {
                mh.readByte();
                Colors color_reset;
                Colors color_green;
                color_green.get_green();

                cout << color_green << "Newsgroup "
                     << title << " created"
                     << color_reset << endl;
                return;
            }
            else
            {
                handle_error(mh.readByte());
                mh.readByte();
                return;
            }
        }

        throw ProtocolViolationException();
    }
    catch (ConnectionClosedException)
    {
        handle_error(SERVERISDOWN);
    }
}

/** This const member sends intention to delete
 *  a newsgroup with a newsgroup id.
 *  @param &mh is a reference to a MessageHandler
 *  @param news_id is an int
 */
void ClientCommand::delete_ng(MessageHandler &mh, int news_id) const
{
    try
    {
        mh.sendByte(Protocol::COM_DELETE_NG);
        mh.sendIntPar(news_id);
        mh.sendByte(Protocol::COM_END);

        if (mh.readByte() == Protocol::ANS_DELETE_NG)
        {
            if (mh.readByte() == Protocol::ANS_ACK)
            {
                Colors color_reset;
                Colors color_green;
                color_green.get_green();

                cout << color_green << "Newsgroup "
                     << news_id << " deleted"
                     << color_reset << endl;
                if (mh.readByte() == Protocol::ANS_END)
                {
                    return;
                }
            }
            else
            {
                handle_error(mh.readByte());
                mh.readByte();
                return;
            }
        }

        throw ProtocolViolationException();
    }
    catch (ConnectionClosedException)
    {
        handle_error(SERVERISDOWN);
    }
}


/** This const member sends intention to list
 *  all articles in a newsgroup with a newsgroup id.
 *  @param &mh is a reference to a MessageHandler
 *  @param news_id is an int
 */
void ClientCommand::list_art(MessageHandler &mh, int news_id) const
{
    try
    {
        mh.sendByte(Protocol::COM_LIST_ART);
        mh.sendIntPar(news_id);
        mh.sendByte(Protocol::COM_END);

        if (mh.readByte() == Protocol::ANS_LIST_ART)
        {
            if (mh.readByte() == Protocol::ANS_ACK)
            {
                Colors color_reset;
                Colors color_green;
                color_green.get_green();

                cout << color_green
                     << "Listing articles in newsgroup " << news_id
                     << color_reset << endl;

                int n = mh.readIntPar();
                if (n == 0) cout << " (none)" << endl;

                for (int i = 0; i != n; ++i)
                {
                    int id = mh.readIntPar();
                    string title = mh.readStringPar();
                    cout << " " << id << ": " << title << endl;
                }
                cout << endl;

                if (mh.readByte() == Protocol::ANS_END) return;
            }
            else
            {
                handle_error(mh.readByte());
                mh.readByte();
                return;
            }
        }

        throw ProtocolViolationException();
    }
    catch (ConnectionClosedException)
    {
        handle_error(SERVERISDOWN);
    }
}

/** This const member sends intention to create an
 *  article in a newsgroup.
 *  @param &mh is a reference to a MessageHandler
 *  @param news_id is an int
 *  @param &title is a reference to a const string
 *  @param &author is a reference to a const string
 *  @param &text is a reference to a const string
 */
void ClientCommand::create_art(MessageHandler &mh, int news_id,
                              const string &title, const string &author,
                              const string &text) const
{
    try
    {
        mh.sendByte(Protocol::COM_CREATE_ART);
        mh.sendIntPar(news_id);
        mh.sendStringPar(title);
        mh.sendStringPar(author);
        mh.sendStringPar(text);
        mh.sendByte(Protocol::COM_END);

        if (mh.readByte() == Protocol::ANS_CREATE_ART)
        {
            if (mh.readByte() == Protocol::ANS_ACK)
            {
                if (mh.readByte() == Protocol::ANS_END)
                {
                    Colors color_reset;
                    Colors color_green;
                    color_green.get_green();

                    cout << color_green
                         << "Article created"
                         << color_reset << endl;
                    return;
                }
            }
            else
            {
                handle_error(mh.readByte());
                mh.readByte();
                return;
            }
        }

        throw ProtocolViolationException();
    }
    catch (ConnectionClosedException)
    {
        handle_error(SERVERISDOWN);
    }
}

/** This const member sends intention to delete an
 *  article in a newsgroup.
 *  @param &mh is a reference to a MessageHandler
 *  @param news_id is an int corresponding to a newsgroup id
 *  @param art_id is an int corresponding to a article id
 */
void ClientCommand::delete_art(MessageHandler &mh, int news_id, int art_id) const
{
    try
    {
        mh.sendByte(Protocol::COM_DELETE_ART);
        mh.sendIntPar(news_id);
        mh.sendIntPar(art_id);
        mh.sendByte(Protocol::COM_END);

        if (mh.readByte() == Protocol::ANS_DELETE_ART)
        {
            if (mh.readByte() == Protocol::ANS_ACK)
            {
                if (mh.readByte() == Protocol::ANS_END)
                {
                    Colors color_reset;
                    Colors color_green;
                    color_green.get_green();

                    cout << color_green
                         << "Article deleted"
                         << color_reset << endl;
                    return;
                }
            }
            else
            {
                handle_error(mh.readByte());
                mh.readByte();
                return;
            }
        }

        throw ProtocolViolationException();
    }
    catch (ConnectionClosedException)
    {
        handle_error(SERVERISDOWN);
    }
}

/** This const member sends intention to get an
 *  article in a newsgroup.
 *  @param &mh is a reference to a MessageHandler
 *  @param news_id is an int corresponding to a newsgroup id
 *  @param art_id is an int corresponding to a article id
 */
void ClientCommand::get_art(MessageHandler &mh, int news_id, int art_id) const
{
    try
    {
        mh.sendByte(Protocol::COM_GET_ART);
        mh.sendIntPar(news_id);
        mh.sendIntPar(art_id);
        mh.sendByte(Protocol::COM_END);

        if (mh.readByte() == Protocol::ANS_GET_ART)
        {
            if (mh.readByte() == Protocol::ANS_ACK)
            {
                string title = mh.readStringPar();
                string author = mh.readStringPar();
                string text = mh.readStringPar();
                print_article(title, author, text);

                if (mh.readByte() == Protocol::ANS_END) return;
            }
            else
            {
                handle_error(mh.readByte());
                mh.readByte();
                return;
            }
        }

        throw ProtocolViolationException();
    }
    catch (ConnectionClosedException)
    {
        handle_error(SERVERISDOWN);
    }
}

/** This int member reads from cin and returns
 *  the first valid int.
 *  @param &prompt is a reference to a const string
 *  @return valid is an int valid to use in this context
 */
int ClientCommand::get_valid_int(const string &prompt)
{
    string input = "";
    int valid;

    while (true)
    {
        cout << prompt << ": ";
        getline(cin, input);
        istringstream myStream(input);
        if (myStream >> valid) break;
    }

    return valid;
}

/** This string member reads from cin and returns
 *  a valid text i.e a message for The news system
 *  @param &prompt is a reference to a const string
 *  @return text is a string valid to use in this context
 */
string ClientCommand::get_valid_text(const string &prompt)
{
    string line;
    ostringstream text;

    cout << prompt << " (end with a single . on a line):" << endl
         << "--------------------------" << endl;

    while (true)
    {
        getline(cin, line);
        if (line == ".") break;
        text << line << endl;
    }

    return text.str();
}

/** This const member gets called with an error code
 *  in order to print the correct error message and or
 *  throw the correct exception
 *  @param errcode is an int corresponding to errors listed in two enums
 *  @throws ConnectionClosedException is an exception
 */
void ClientCommand::handle_error(int errcode) const
{
    Colors color_reset;
    Colors color_yellow;
    color_yellow.get_yellow();
    Colors color_red;
    color_yellow.get_red();

    switch (errcode)
    {
    case Protocol::ERR_NG_ALREADY_EXISTS:
        cout << color_yellow
             << "Usage error: newsgroup already exists"
             << color_reset << endl;
        break;
    case Protocol::ERR_NG_DOES_NOT_EXIST:
        cout << color_yellow
             << "Usage error: newsgroup does not exist"
             << color_reset << endl;
        break;
    case Protocol::ERR_ART_DOES_NOT_EXIST:
        cout << color_yellow
             << "Usage error: article does not exist"
             << color_reset << endl;
        break;
    case TOSHORTTITLE:
        cout << color_yellow
             << "Usage error: no title supplied"
             << color_reset << endl;
        break;
    case TOSHORTAUTHOR:
        cout << color_yellow
             << "Usage error: no author supplied"
             << color_reset << endl;
        break;
    case SERVERISDOWN:
        cerr << color_red
             << "Connection with server lost"
             << color_reset << endl;
        throw ConnectionClosedException();
        break;
    default:
        cerr << color_red
             << "Unknown error"
             << color_reset << endl;
    }
}

