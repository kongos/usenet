#ifndef PROTOCOLVIOLATIONEXCEPTION_H
#define PROTOCOLVIOLATIONEXCEPTION_H

/*
 * In namespace protocol
 */
namespace protocol
{

/**  ProtocolViolationException is thrown
 *   when the protocol is violated
 */
struct ProtocolViolationException {};
}

#endif
