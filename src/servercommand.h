#ifndef SERVERCOMMAND_H
#define SERVERCOMMAND_H

#include "database.h"
#include "connection.h"
#include "messagehandler.h"

using client_server::Connection;

class ServerCommand
{
public:
    ServerCommand(Database *adb) : db(adb) {};
    bool handle(Connection *conn) const;
private:
    Database *db;

    bool handle_listng(MessageHandler &mh) const;
    bool handle_createng(MessageHandler &mh) const;
    bool handle_deleteng(MessageHandler &mh) const;
    bool handle_listart(MessageHandler &mh) const;
    bool handle_createart(MessageHandler &mh) const;
    bool handle_deleteart(MessageHandler &mh) const;
    bool handle_getart(MessageHandler &mh) const;
};

#endif
