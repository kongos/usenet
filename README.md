# Usenet implementation in C++ with CLI interface
 * 'newsclient' is a terminal based news reader client
 * 'newsserver_mem' is a server with an in-memory database
 * 'newsserver_db' is a server with a persistent on-disk database


##Building

Simply run `make` to build all binaries.
They will be located in the `bin/` directory.

To remove all object files
run `make clean`. 

To remove object files, libraries, binaries and bin/
run `make cleaner`.


##Usage

Run the binaries without arguments to get usage help.

The port number you select must be above `1024`.

If connecting the client to a server running on the same
computer, the IP `127.0.0.1` or string `localhost` can be used. 
Otherwise you must specify the IP to the computer running the server.

##Exiting the client

To cleanly exit the **client** a user can press `CTRL + D` whenever the **Menu** 
prompt shows, a user can invoke `CTRL + D` at other places aswell however 
this might yield an error message depending on at what place execution
stopped.

##Exiting the newsserver_mem

To cleanly exit the **newsserver_mem** it's just a matter of invoking `CTRL + C`

##Exiting the newsserver_db


To cleanly exit the **newsserver_db** it's just a matter of invoking `CTRL + C`

##Documentation 

To gain documentation run `doxygen Doxyfile` after this the `documentation.html` will take you there
