# Enter the "src" directory and does make there

.PHONY: all clean cleaner

all:
	make -C src all
clean:
	make -C src clean
cleaner:
	make -C src cleaner
